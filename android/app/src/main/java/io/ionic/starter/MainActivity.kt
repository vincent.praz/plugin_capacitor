package io.ionic.starter

import android.os.Bundle
import androidx.compose.foundation.Text
import androidx.compose.ui.platform.setContent
import com.getcapacitor.BridgeActivity
import com.getcapacitor.Plugin
import com.getcapacitor.PluginMethod

// Import plugin
import com.plugin.rendu.PluginRendu



import java.util.*


class MainActivity : BridgeActivity() {

    @PluginMethod()
    public fun showText() {
        setContent {
            Text(text = "HELLO ADVENTURER")
        }
    }

    var currentLatitude = 0
    var currentLongitude = 0
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initializes the Bridge
        this.init(savedInstanceState, object : ArrayList<Class<out Plugin?>?>() {
            init {
                // Additional plugins you've installed go here
                add(PluginRendu::class.java)
            }
        })

        if (savedInstanceState != null) {
            with(savedInstanceState) {
                currentLatitude = getInt(STATE_LATITUDE)
                currentLongitude = getInt(STATE_LONGITUDE)
            }
        }

//        setContent {
//            Text(text = "HELLO ADVENTURER")
//        }

//        add(PluginRendu::class.java)

//        add(PluginRendu::class.java)


    }

    // Save lat and long
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(STATE_LATITUDE, currentLatitude)
            putInt(STATE_LONGITUDE, currentLongitude)
        }
        super.onSaveInstanceState(outState)
    }

    // Retrieve last lat and long
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState.run {
            currentLatitude = getInt(STATE_LATITUDE)
            currentLongitude = getInt(STATE_LONGITUDE)
        }
    }


    companion object {
        val STATE_LATITUDE = "Latitude"
        val STATE_LONGITUDE = "Longitude"
    }
}
