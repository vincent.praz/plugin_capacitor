import { Component } from '@angular/core';
import { DataService, Message } from '../services/data.service';
import {Device, DeviceInfo} from '@capacitor/core';
import { Geolocation} from '@capacitor/core';

// import { Plugins, AppState } from '@capacitor/core';
// const { app } = Plugins;

import { Plugins } from '@capacitor/core';

const { PluginRendu } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public info: string;
  public latitude: number;
  public longitude: number;
  contacts = [];

  constructor(private data: DataService) {
    // this.getLocation();
  }

  refresh(ev) {
    setTimeout(() => {
      ev.detail.complete();
    }, 3000);
  }

  getMessages(): Message[] {
    return this.data.getMessages();
  }

  async launchPlugin() {
    // this.info = JSON.stringify(await Device.getInfo());
    // console.log(this.info);
    // PluginName.MainActivity();
    this.contacts = (await PluginRendu.getContacts('Driss')).results;
    console.log('contacts', this.contacts);
  }

  async getLocation() {
    const position = await Geolocation.getCurrentPosition();
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;
  }
}
